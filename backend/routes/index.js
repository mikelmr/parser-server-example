const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const Todo = require("../models/todos/todo");
const Parse = require('parse/node');

const routes = (app) => {
  const router = express.Router();

  router.post("/todos", async(req, res) => {
    const todo = new Todo(req.body.text);

    try {
        const result = await todo.save();
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, result);
      } catch (e) {
        serverResponses.sendError(res, messages.BAD_REQUEST, e);
      }
  });

  router.get("/", async(req, res) => {
    try {
        const todos = await Todo.getAll();
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, todos);
    } catch (error) {
        serverResponses.sendError(res, messages.BAD_REQUEST, e);
    }
  });

  //it's a prefix before api it is useful when you have many modules and you want to
  //differentiate b/w each module you can use this technique
  app.use("/api", router);
};
module.exports = routes;
