const Parse = require('parse/node');

class Todo {
  constructor(text) {
    this.text = text;
  }

  save() {
    const Todo = Parse.Object.extend('todos');
    const todo = new Todo();
    todo.set('text', this.text);
    
    return todo.save();
  }

  static async getAll() {
    const Todo = Parse.Object.extend('todos');
    const query = new Parse.Query(Todo);
    
    try {
      const todos = await query.find();
      return todos.map(todo => ({
        id: todo.id,
        text: todo.get('text')
      }));
    } catch (error) {
      console.error('Error fetching todos', error);
      return [];
    }
  }
}

module.exports = Todo;