/**
 * Created by Syed Afzal
 */
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

const Parse = require('parse/node');
Parse.serverURL = 'http://parse-server:1337/parse/';
Parse.initialize("z", "somemasterkey");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//  adding routes
require("./routes")(app);

app.listen(3000, () => {
    console.log("Server is up on port", 3000);
});

module.exports = app;
